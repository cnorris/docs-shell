# Script for ZSH

# Test files (docs test files)
test_install() {
  echo 'install/install.sh ok'
}

# Test function
hello() {
  printf "Hello $USER! $WAVE \n"
}

# ---------------------------------------------------------------- #

# Install Docs Shell

# Initial command: `. ./docs.sh && access`

# Access
access(){
  tput setaf 5 ; printf "Granting shell access to Docs Shell... \n" ; tput sgr0
  echo "Granting terminal execution... \n`tput bold`$ chmod +x $CURRENT/docs.sh`tput sgr0`" ; chmod +x $CURRENT/docs.sh
  echo "Exporting path to terminal... \n`tput bold`$ export PATH=$PATH:$CURRENT`tput sgr0`" ; export PATH=$PATH:$CURRENT
  THIS=$CURRENT
  # Load custom alias file
  echo "Source temporary alias functions/assets/alias.zsh" ; source functions/assets/alias.zsh
  add_permanent_alias
}

# Permanent alias
perm_alias() {
  echo 'alias docs="cd '$THIS' && . ./docs.sh && "'
}

# Add permanent alias
add_permanent_alias() {
  read  "REPLY?Would you like add a permanent alias (y/n)?"
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    if [[ $SHELL == '/bin/zsh' ]]; then
      printf "\n$(perm_alias)" >> ~/.zshrc
      echo 'Alias added to ~/.zshrc'
      read  "REPLY?You need to relaunch for changes to take effect. Would you like to close this shell window now (y/n)?"
        if [[ $REPLY =~ ^[Yy]$ ]] ; then
          exit 0
        fi
    else
      echo "Sorry, you have to add the alias according to your shell (`tput bold`$SHELL`tput sgr0`)."
    fi
  else
    tput bold ; echo "You'll have to run \`\$ source functions/assets/alias.zsh\` every time you open the terminal to use \`docs\` command." ; tput sgr0
    echo "If you'd like to add the permanent alias, run \`\$ docs add_permanent_alias\`."
  fi
}

first_compile(){
  go_docs
  echo `tput setaf 5`"Bundling site dependecies..." `tput sgr0`
  echo `tput bold`"$ bundle "_"$DBUN"_" install --quiet"`tput sgr0` ; bundle '_'$DBUN'_' install --quiet
  echo `tput setaf 5`"Compiling the docs site..." `tput sgr0`
  echo `tput bold`"$ bundle exec nanoc compile" `tput sgr0`; # bundle exec nanoc compile
  ifsuccess
  echo `tput setaf 5`"Starting live preview..." `tput sgr0`
  echo `tput bold`"$ bundle exec nanoc live -p $PORT" `tput sgr0`; bundle exec nanoc live -p $PORT | open_preview
}

# Install `docs install`

install() {
  printf "Hello `tput setaf 5`$USER`tput sgr0`! ${WAVE}\nBefore we begin the installation process, make sure your terminal is in the docs-shell directory.\n"
  echo "If you aren't sure, abort the process by pressing control + c on your keyboard and run `tput bold`$ docs install`tput sgr0` when you're ready."
  # Clone repos or setup current paths and create symlinks
  tput setaf 3 ; read "REPLY?Do you have `tput bold`all repos already cloned`tput sgr0``tput setaf 3` (GitLab [or GDK], Omnibus, Runner, Charts, Docs) (y/n)?`tput sgr0`"
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      paths
    else
      clone
    fi
  # Dependencies
  echo `tput setaf 5`"Dependencies check:" `tput sgr0`
  check_dependencies
  if [[ $? == 0 ]] ; then
    tput setaf 2; printf "${UNICORN} Dependencies are ready! `tput sgr0` (${NOW}) \n"
  fi
  # Compile and preview
  first_compile
  # Installation log file
  echo "Installed on ${NOW}" > $DSHELL/.log
}

update_shell() {
  go_shell
  git_checkout_master
  git pull origin master
  source config.sh
}
