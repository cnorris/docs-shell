# Script for ZSH

# Test file (docs test files)
test_install_dep() {
  echo 'install/dependencies.sh ok'
}

# ---------------------------------------------------------------- #

# Xcode Select (macOS)

depcheck_xcode () {
  echo `tput setaf 5`"Checking xcode command line tools (macOS)..." `tput sgr0`
  echo  `tput bold` '$ xcode-select --version' `tput sgr0` ; xcode-select --version
}

check_xcode() {
  if ! depcheck_xcode ; then
    tput setaf 3
    error_sound ; read "REPLY?Required on macOS: Would you like to install Apple's XCode command line tools (y/n)?"
    tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      echo `tput bold` '$ xcode-select --install' `tput sgr0` ; xcode-select --install
    elif [[ $REPLY =~ ^[Nn]$ ]] ; then
      tput setaf 3
      read "REPLY1?Are you on macOS (y/n)?"
      tput sgr0
      if [[ $REPLY1 =~ ^[Yy]$ ]] ; then
        echo `tput setaf 1` 'Failed. xcode-select is required. \n' `tput sgr0` 'See https://osxdaily.com/2014/02/12/install-command-line-tools-mac-os-x/ for installation instructions.'
        false
      elif [[ $REPLY1 =~ ^[Nn]$ ]] ; then
        echo 'Ok, going ahead.'
      fi
    fi
  fi
}

# ---------------------------------------------------------------- #

# RubyGems

depcheck_rubygems () {
  echo 'Checking RubyGems...'
  echo `tput bold` '$ gem --version' `tput sgr0` ; gem --version
}

check_ruby_gems() {
  if ! depcheck_rubygems ; then
    echo `tput setaf 1`'Failed. RubyGems is required. \n' `tput sgr0` 'See https://rubygems.org/pages/download for installation instructions.'
    echo `tput setaf 1`'Aborting.'`tput sgr0`
    false
  fi
}

# ---------------------------------------------------------------- #

# Bundler

# Check bundler version for GitLab Docs
bundler_version() {
  awk '/BUNDLED WITH/{getline; $1=$1 ; print}' $DOC/Gemfile.lock
}

docs_bundler() {
  DBUN=$(bundler_version)
  printf "$DBUN\n" > .bundler
}

latest_docs() {
  pull docs >/dev/null
}

depcheck_bundler() {
  docs_bundler
  echo `tput setaf 5`"Checking Bundler..." `tput sgr0`
  echo  `tput bold` '$ gem list -i bundler --version '$(bundler_version)'' `tput sgr0`; gem list -i bundler --version $(bundler_version) >/dev/null
}

check_bundler() {
  if depcheck_bundler ; then
    tput setaf 2; printf "Bundler $DBUN is installed. `tput sgr0` \n"
  else
    error_sound ; echo `tput setaf 1`"Failed. Bundler $DBUN is required. Installing..."`tput sgr0`
    echo `tput bold` "$ gem install bundler:$DBUN" `tput sgr0` ; gem install bundler:$DBUN
  fi
}

# ---------------------------------------------------------------- #

# Homebrew

depcheck_brew() {
  echo `tput setaf 5`"Checking Homebrew..." `tput sgr0`
  echo  `tput bold` '$ brew --version' `tput sgr0` ; brew --version
}

check_brew() {
  if ! depcheck_brew ; then
    echo `tput setaf 1`'Failed. Homebrew is required. Installing...'`tput sgr0`
    echo `tput bold` '$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"'
    `tput sgr0` ; /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
  fi
}

# ---------------------------------------------------------------- #

# Node

depcheck_node() {
  echo `tput setaf 5`"Checking Node..." `tput sgr0`
  echo  `tput bold` '$ node --version' `tput sgr0` ; node --version
}

check_node() {
  if ! depcheck_node ; then
    echo `tput setaf 1`'Failed. Node is required.'`tput sgr0`
    echo `tput bold`'Recommended: '`tput sgr0`'install Node with a version manager (nvm). See https://github.com/nvm-sh/nvm#installation-and-update for installation instructions.'
    echo `tput bold`'Alternative 1: '`tput sgr0`'install Node directly. See https://nodejs.org/en/download/ for installation instructions.'
    install_node
  fi
}

# Install Node with Homebrew

install_node() {
  tput setaf 3
  echo `tput bold`'Alternative 2:'`tput sgr0`'Install Node with Homebrew.'
  error_sound ; tput setaf 3 ; read "REPLY?Would you like to install NodeJS with Homebrew now (y/n)?"; tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    echo  `tput bold` '$ brew install node' `tput sgr0` ; brew install node
  else
    echo `tput setaf 1`'Failed: you need Node JS. Aborting.'`tput sgr0`
    false
  fi
}

# ---------------------------------------------------------------- #

# Git

depcheck_git () {
  echo `tput setaf 5`"Checking Git..." `tput sgr0`
  echo `tput bold` '$ git --version' `tput sgr0` ; git --version
}

check_git() {
  if ! depcheck_git ; then
    echo `tput setaf 1`'Failed. Git is required. Installing...'`tput sgr0`
    echo `tput bold` '$ brew install git' `tput sgr0` ; brew install git
  fi
}

# ---------------------------------------------------------------- #

# OpenSSL

depcheck_openssl () {
  echo `tput setaf 5`"Checking OpenSSL..." `tput sgr0`
  echo `tput bold` '$ openssl version' `tput sgr0` ; openssl version
}

check_openssl() {
  if ! depcheck_openssl ; then
    echo `tput setaf 1`'Failed. OpenSSL is required. Installing...'`tput sgr0`
    echo `tput bold` '$ brew install openssl' `tput sgr0` ; brew install openssl
  fi
}

# ---------------------------------------------------------------- #

# Nanoc

depcheck_nanoc() {
  cd $DOC
  echo `tput setaf 5`"Checking Nanoc dependencies..." `tput sgr0`
  echo `tput bold` "$ bundle "_"$DBUN"_" install --quiet" `tput sgr0` ; bundle '_'$DBUN'_' install --quiet
  echo `tput setaf 5`"Checking Nanoc..." `tput sgr0`
  echo `tput bold` '$ bundle exec nanoc --version' `tput sgr0` ; bundle exec nanoc --version
  cd $DSHELL
}

check_nanoc() {
  if ! depcheck_nanoc ; then
    echo `tput setaf 1`'Failed. Nanoc is required. Installing...'`tput sgr0`
    echo `tput bold` '$ gem install nanoc' `tput sgr0` ; gem install nanoc
  fi
}

# ---------------------------------------------------------------- #

# Yarn

depcheck_yarn() {
  echo `tput setaf 5`"Checking Yarn..." `tput sgr0`
  echo  `tput bold` '$ yarn --version' `tput sgr0` ; yarn --version
}

check_yarn() {
  if ! depcheck_yarn ; then
    echo `tput setaf 1`'Failed. Yarn is required. Installing...'`tput sgr0`
    echo `tput bold` '$ brew install yarn' `tput sgr0` ; brew install yarn
    yarn_install
  else 
    yarn_install
  fi
}

yarn_install() {
  cd $DOC
  echo `tput setaf 5`"Installing Yarn dependencies..." `tput sgr0`
  echo `tput bold` '$ yarn install --silent' `tput sgr0` ; yarn install --silent
  cd $DSHELL
}

# ---------------------------------------------------------------- #

# Check dependencies

check_dependencies() {
  latest_docs
  # xcode-select
  check_xcode
  # ruby
  echo `tput setaf 5`"Checking rubies..." `tput sgr0`
  check_rvm
  check_ruby
  # Rubygems
  check_ruby_gems
  # Bundler
  check_bundler
  # Homebrew
  check_brew
  # Node
  check_node
  # Git
  check_git
  # Check OpenSSL
  check_openssl
  # Nanoc
  check_nanoc
  # Yarn
  check_yarn
  # Linters
  check_mdlint
  check_vale
  check_yamllint
  bel
}

# Check/update dependencies
dep(){
  if [[ $1 == '--update' ]]; then
    tput setaf 5 ; echo "Updating dependencies..." ; tput sgr0
    update_dep
    tput setaf 5 ; echo "Double-checking dependencies..." ; tput sgr0
    check_dependencies
  else
    tput setaf 5 ; echo "Checking dependencies..." ; tput sgr0
    check_dependencies
  fi
  cd $DSHELL
}
