# Test files (docs test files)
test_preinstall() {
  echo 'install/pre-install.sh ok'
}

# ---------------------------------------------------------------- #

# Pre-install

pre-install() {
  check_brew
  check_zsh
  check_iterm2
  check_oh-my-zsh
}

# ---------------------------------------------------------------- #

# Install ZSH

depcheck_zsh() {
  echo `tput setaf 5`"Checking ZSH..." `tput sgr0`
  echo  `tput bold` '$ zsh --version' `tput sgr0` ; zsh --version
  if [[ $? == 0 ]]; then
    tput setaf 2 ; echo 'Success: ZSH is installed.' ; tput sgr0
  fi
}

check_zsh() {
  if ! depcheck_zsh ; then
    echo `tput setaf 1`'Failed. ZSH is required.'`tput sgr0`
    error_sound ; tput setaf 3 ; read "REPLY?Would you like to install ZSH with Homebrew (y/n)?"; tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      install_zsh
    else
      echo 'Ok. Aborting.'
    fi
  fi
}

install_zsh() {
  echo `tput setaf 5`"Installing ZSH..." `tput sgr0`
  echo  `tput bold` '$ brew install zsh' `tput sgr0` ; brew install zsh
  echo `tput setaf 5`"Sourcing ZSH..." `tput sgr0`
  echo  `tput bold` '$ source ~/.zhrc' `tput sgr0` ; source ~/.zhrc
}

# ---------------------------------------------------------------- #

# Install iTerm2

check_iterm2() {
  echo `tput setaf 5`"Checking iTerm2..." `tput sgr0`
  if [ -e ~/Library/Preferences/com.googlecode.iterm2.plist ]
  then
    tput setaf 2 ; echo "Success: iTerm2 is installed." ; tput sgr0
  else
    echo `tput setaf 1`'Failed. iTerm2 is not installed.'`tput sgr0`
    install_iterm
  fi
}

install_iterm() {
  ok_sound ; tput setaf 3 ; read "REPLY?Would you like to install iTerm2 (not required) (y/n)?"; tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    echo  `tput bold` '$ brew cask install iterm2' `tput sgr0` ; brew cask install iterm2
  else
    echo 'Ok. Aborting.'
  fi
}

# ---------------------------------------------------------------- #

# Install oh-my-zsh

check_oh-my-zsh() {
  echo `tput setaf 5`"Checking oh-my-zsh..." `tput sgr0`
  if [ -d ~/.oh-my-zsh ]
  then
    tput setaf 2 ; echo "Success: oh-my-zsh is installed." ; tput sgr0
  else
    echo `tput setaf 1`'Failed. oh-my-zsh is not installed.'`tput sgr0`
    install_ohmyzsh
  fi
}

install_ohmyzsh() {
  ok_sound ; tput setaf 3 ; read "REPLY?Would you like to install oh-my-zsh (not required) (y/n)?"; tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    echo `tput bold`'$ sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"' `tput sgr0` ; sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
  else
    echo 'Ok. Aborting.'
  fi
}
