# Commands reference

`docs [option]` commands available:

```bash
# Test or trigger `docs`:
docs hello

# Docs Shell usage:
docs --help

# Installation:
docs install # option to clone all repos or set up custom paths
docs pre-install # option to check and install ZSH, iTerm2, oh-my-zsh

# Change paths:
docs paths # Change custom paths
docs paths --reset # reset to default (cloned) paths

# Check dependencies:
docs dep
# Update and check dependencies:
docs dep --update

# Lint:
docs lint # lints both content and links
# - Lint options:
docs lint --content | -c # lint content
docs lint --nanoc   | -n # lint gitlab-docs

# Lint content options:
docs lint -c [repo]
# - Example:
docs lint -c gitlab
# - Lint content repo options:
docs lint -c [repo] [linter]
# - Examples:
docs lint -c gitlab markdown
docs lint -c gitlab vale

# Lint Nanoc options:
docs lint -n
# Options:
docs lint -n anchors # lint for broken anchors
docs lint -n links # lint for broken internal links
docs lint -n elinks # lint for broken external links

# gitlab-docs development linters
docs lint --dev | -d # lint dev linters
docs lint -d [linter] # lint specific linter
# Examples:
docs lint -d yaml
docs lint -d scss

# Compile:
docs compile
docs compile live # Compile and live preview
docs compile live lint # Compile, live preview, lint

# Preview GitLab Docs:
docs live

# Recompile:
docs recompile
docs recompile lint # recompile and lint content and nanoc
docs recompile lint live # recompile, lint content and nanoc, preview docs

# Pull repos
docs pull # checkout master, pull all repos (housekeeping)
docs pull [repo] [repo] ... [repo] # checkout master, pull
# - Examples:
docs pull gitlab # pull GitLab
docs pull gitlab runner # Pull GitLab and GitLab Runner
docs pull docs # Pull GitLab Docs

# Similar to `docs pull` commands, more granular
docs update
docs update --skip # updates all repos except GitLab
docs update -h # (housekeeping)
docs update -h -s # (housekeeping + stash EE)
docs update -a # (all doc/ repos quick + gitlab-docs)
docs update -h -a #(housekeeping all)

# Reset repository (checkout master and git reset --hard origin/master)
docs reset [repo]
Examples:
docs reset gitlab # resets `gitlab`
docs reset gitlab docs # resets `gitlab` and `docs`

# Nanoc direct commands
docs nanoc compile
docs nanoc live

# Kill nanoc live
docs kill

# Navigation
docs go_gitlab
docs go_docs
docs go_runner
docs go_omnibus
docs go_charts
docs go_shell
```
