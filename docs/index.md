# Docs Shell documentation

- For brief installation steps, see [installation](README.md#installation).
  For further installation info, cloning repos, reset paths, and troubleshooting, see [installation details](installation.md).
- For Useful information and references for the development of this project,
see [info](info.md).

## Workflows

See the most common workflows below.

### Getting started

To get started, choose one of the options below.

a. To pull all repos and compile GitLab Docs:

  ```bash
  docs pull
  docs compile
  ```

b. To pull all repos, compile GitLab Docs, and run live preview:

  ```bash
  docs pull
  docs compile live
  ```

c. To pull all repos, compile GitLab Docs, run live preview, and run all linters:

  ```bash
  docs pull
  docs compile live lint
  ```

**Note:** if you installed Docs Shell with GDK, preview docs on
http://localhost:3005. Otherwise, on http://localhost:3000.

### Changing content

Suppose you want to work on a GitLab document. The same process is valid
for Runner, Omnibus, and Charts docs (replacing `gitlab` with `omnibus`,
`runner`, or `charts` in the commands below).

1. Pull all repos and start docs live preview:

   ```bash
   docs pull
   docs compile live
   ```

1. Checkout a new branch in `gitlab`:

   ```bash
   docs go_gitlab
   git checkout -b my-new-branch
   ```

1. Change the content of the files you want.
1. Recompile:

   ```bash
   docs recompile
   ```

1. Go to to your browser and preview your changes.
1. To lint the content (markdownlint and Vale):


   ```bash
   docs lint -c gitlab
   ```

1. To lint only Vale:


   ```bash
   docs lint -c gitlab vale
   ```

1. To lint only markdownlint:


   ```bash
   docs lint -c gitlab markdown
   ```

1. Add, commit, and push your changes:

   ```bash
   docs go_gitlab
   git add .
   git commit -m "Add a commit message"
   git push origin my-new-branch
   ```

1. Open a merge request from `my-new-branch` to `master`.

### One step at a time

1. To pull all repos:

   ```bash
   docs pull
   ```

1. To pull only one repo:

   ```bash
   docs pull gitlab
   ```

   Also available: `docs pull runner`, `docs pull omnibus`, `docs pull charts`, `docs pull docs` (pull GitLab Docs), and `docs pull shell` (Pull Docs Shell).

1. To compile GitLab Docs:

   ```bash
   docs compile
   ```

1. To preview GitLab Docs:

   ```bash
   docs live
   ```

1. To reset one or more repos (`git reset --hard`):

   ```bash
   docs reset gitlab
   ```

   Also available: `docs reset runner`, `docs reset omnibus`, `docs reset charts`, `docs reset docs`, and `docs reset shell`. You can also reset more than one repo, for example: `docs reset gitlab docs`.

#### Linting

1. To lint all repos (content, links, and anchors):

   ```bash
   docs lint
   ```

1. To lint only content (Vale and markdownlint):

   ```bash
   docs lint -c
   ```

1. To lint content in only one repo:

   ```bash
   docs lint -c gitlab
   ```

1. To lint only Vale in one repo:

   ```bash
   docs lint -c gitlab vale
   ```

1. To lint only markdownlint in one repo:

   ```bash
   docs lint -c gitlab markdown
   ```

1. To lint Nanoc for broken links and anchors:

   ```bash
   docs lint -n
   ```

1. To lint only anchors:

   ```bash
   docs lint -n anchors
   ```

1. To lint only internal links:

   ```bash
   docs lint -n links
   ```

1. To lint only external links:

   ```bash
   docs lint -n elinks
   ```

### Check dependencies

To check GitLab Docs dependencies, see the steps below.

1. Pull GitLab Docs:

   ```shell
   docs pull docs
   ```

1. Check the dependencies:

   ```shell
   docs dep
   ```

To update all dependencies:

1. Pull GitLab Docs:

   ```shell
   docs pull docs
   ```

1. Update all dependencies:

   ```shell
   docs dep --update
   ```

## Docs site (docs.gitlab.com) development

1. Pull all repos:

   ```bash
   docs pull
   ```

1. Compile GitLab Docs:

   ```bash
   docs compile
   ```

1. Lint Nanoc (broken internal and external links and anchors):

   ```bash
   docs lint -n
   ```

1. Run all linters for gitlab-docs development:

   ```bash
   docs lint -d
   ```

   You can also lint them one by one:

   ```bash
   docs lint -d yarn
   docs lint -d jest
   docs lint -d scss
   docs lint -d yaml
   docs lint -d rspecs
   ```

## Navigate between repos

1. Open the terminal and run `docs hello`. This will bring you to the Docs Shell directory.
1. Run `docs go_gitlab` to open the GitLab repo. Also available: `go_docs`, `go_shell`, `go_omnibus`, `go_runner`, `go_charts`.

## Interrupt

To interrupt a running job, press <kbd>control</kbd> + <kbd>C</kbd> on your
keyboard.

Run `docs kill` to kill `nanoc live` if you have it running on the background.

## Tips

### Open directory in a terminal window

On macOS, to open a given directory in a terminal window:

1. Open Finder side-by-side with a terminal window.
1. Type `cd ` (note the blank space after `cd`).
1. Drag the folder from Finder and drop into the terminal window.
1. You'll see `cd path/to/your/folder` on the terminal.
1. Press <kbd>return</kbd>. Your terminal is now in your directory.

Alternatively:

1. Open Finder and right-click the folder you want to open in a
terminal window.
1. On the menu, click **New terminal at folder**. Or, depending
on your computer's configuration, go to **Services > New terminal at folder**.
1. A new terminal window will open in that directory.

### Retrieve the full path to a directory

On Unix, to retrieve the pull path of a particular directory:

1. Open the terminal in the directory you want to retrieve the path to.
1. Type `pwd` in the terminal and press enter.
1. The output will be the full path to your directory.

